package org.fmavlyutov;

import static org.fmavlyutov.constant.CommandLineConstant.*;

public class Application {

    public static void main(String[] args) {
        displayHello();
        displayArgs(args);
        displayGoodbye();
    }

    private static void displayHello() {
        System.out.println("-----HELLO-----\n");
    }

    private static void displayGoodbye() {
        System.out.println("-----GOODBYE-----");
    }

    private static void displayHelp() {
        System.out.printf("%s - display command line arguments\n", HELP);
        System.out.printf("%s - display program version\n", VERSION);
        System.out.printf("%s - display info about author\n\n", ABOUT);
    }

    private static void displayVersion() {
        System.out.println("Version: 1.0.1\n");
    }

    private static void displayAbout() {
        System.out.println("Author: Philip Mavlyutov\n");
    }

    private static void displayArgs(String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            switch (arg) {
                case HELP:
                    displayHelp();
                    break;
                case VERSION:
                    displayVersion();
                    break;
                case ABOUT:
                    displayAbout();
                    break;
                default:
                    System.out.println("Incorrect argument. Enter [help] to see all arguments\n");
            }
        }
    }

}
